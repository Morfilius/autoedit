<?php


namespace app;

use app\Interfaces\PathResolverInterface;
use app\Interfaces\ViewInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;
use function ob_get_clean;

class TemplateRenderer implements ViewInterface
{
    /**
     * @var ResponseInterface
     */
    private $response;
    /**
     * @var PathResolverInterface
     */
    private $pathResolver;

    /**
     * TemplateRenderer constructor.
     * @param PathResolverInterface $pathResolver
     * @param ResponseInterface $response
     */
    public function __construct(PathResolverInterface $pathResolver, ResponseInterface $response)
    {
        $this->response = $response;
        $this->pathResolver = $pathResolver;
    }

    public function render($view,array $args = array(),$status = 200,$ext = 'php') : ResponseInterface
    {

        $response = new Response();

        ob_start();
        if(!empty($args)){
            extract($args);
        }
        require $this->pathResolver->getViewsPath().$view.'.'.$ext;
        $response->getBody()->write(ob_get_clean());

        return $response->withStatus($status);
    }

    public function raw($str,$status = 200)
    {
        $response = new Response();
        $response->getBody()->write($str);
        return $response->withStatus($status);
    }

    public function json(array $array, $status = 200)
    {
        $response = new Response();
        $response->getBody()->write(json_encode($array));
        return $response->withAddedHeader('content-type', 'application/json')->withStatus($status);
    }
}