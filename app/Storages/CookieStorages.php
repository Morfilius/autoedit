<?php


namespace app\Storages;


use app\Interfaces\StorageInterface;

class CookieStorages implements StorageInterface
{
    public function save($name)
    {
        // TODO: Implement save() method.
    }

    public function get($name)
    {
        if ($this->has($name)) {
            return $_COOKIE[$name];
        }

        return false;
    }

    public function delete($name)
    {
        // TODO: Implement delete() method.
    }

    public function has($name)
    {
        return isset($_COOKIE[$name]);
    }
}