<?php


namespace app;

class EditStream
{

    /**
     * @param $str
     * @return string
     */
    public function edit($str)
    {
        $str = html_entity_decode($str);

        $str = str_replace("</p>", "</p>".PHP_EOL, $str);
        $str = strip_tags($str);

        $str = preg_replace('/[\n]+/s',"\n",$str);


        $str = str_replace(" "," ",$str);
        $str = str_replace("\"<", "<", $str);




        $str = preg_replace('/[:.!?]\s*?\"\s*([А-Я].*?)\"/us',"\n- $1",$str);
        $str = preg_replace('/[:.!?]\s*?\'\s*([А-Я].*?)\'/us',"\n- $1",$str);
        $str = preg_replace('/[:.!?]\s*?«\s*([А-Я].*?)»/us',"\n- $1",$str);
        $str = preg_replace('/[:.!?]\s*?“\s*([А-Я].*?)”/us',"\n- $1",$str);

        $str = preg_replace('/\n\s*\"\s*([А-Я].*?)\"/us',"\n- $1",$str);
        $str = preg_replace('/\n\s*\'\s*([А-Я].*?)\'/us',"\n- $1",$str);
        $str = preg_replace('/\n\s*«\s*([А-Я].*?)»/us',"\n- $1",$str);
        $str = preg_replace('/\n\s*“\s*([А-Я].*?)”/us',"\n- $1",$str);


        $str = str_replace("\r\n", "\n", $str);
        $str = preg_replace('/[\n]+[\s]+/s',"\n",$str);
        $str = preg_replace('/[\n]+/s',"\n",$str);

        $str = str_replace("\n - ", "\n- ", $str);

        $str = str_replace("「", "- ", $str);
        $str = str_replace("」", "", $str);
        $str = str_replace(" • ", " ", $str);
        $str = str_replace("•", "", $str);
        ;

        $str = str_replace("&ldquo;", " - ", $str);
        $str = str_replace("&lsquo;", " - ", $str);
        $str = str_replace("&ndash;", " - ", $str);
        $str = str_replace("&mdash;", " - ", $str);
        $str = str_replace("&laquo;", " - ", $str);
        $str = str_replace("&quot;", "", $str);
        $str = str_replace("&hellip;", "...", $str);
        $str = str_replace("&rdquo;", "", $str);
        $str = str_replace("&raquo;", "", $str);
        $str = str_replace("&amp;", "", $str);
        $str = str_replace("#39;", "", $str);
        $str = str_replace("\n ", "\n", $str);
        $str = str_replace("&", "", $str);


        return $str;
    }
}