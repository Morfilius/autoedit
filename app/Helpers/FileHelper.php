<?php


namespace app\Helpers;


class FileHelper
{
    public static function getFileExt($fileName)
    {
        preg_match('/\.[\w]{3}$/u',$fileName,$match);
        return str_replace('.', '', reset($match));
    }
}