<?php


namespace app;


use Exception;

class AggregateFile
{
    private $pathDir;
    private $content;
    private $filename;

    /**
     * AggregateFile constructor.
     * @param $pathDir
     * @param $content
     * @param $filename
     */
    public function __construct($pathDir, $content, $filename = 'text.txt')
    {
        $this->pathDir = $pathDir;
        $this->content = $content;
        $this->filename = $filename;
    }

    /**
     * @return bool|int
     * @throws Exception
     */
    public function aggregate()
    {
        $results = $this->save();
        if ($results) {
            return $results;
        } else {
          throw new Exception('Failed to append data to the resulting file.');
        }
    }

    public function getFilePath()
    {
        return $this->pathDir.$this->filename;
    }

    public function getResultFilename()
    {
        return $this->filename;
    }

    private function save()
    {
        return file_put_contents($this->getFilePath(),$this->content,FILE_APPEND);
    }

}