<?php


namespace app\Interfaces;


use app\AbstractClasses\AbstractTextHandler;

interface TextHandlerFactoryInterface
{
    public function create($fileExtension) : AbstractTextHandler;
}