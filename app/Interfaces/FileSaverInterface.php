<?php


namespace app\Interfaces;


interface FileSaverInterface
{
    public function save(array $fileGlobEl);
    public function delete($fileName);
}