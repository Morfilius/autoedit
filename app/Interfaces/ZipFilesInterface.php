<?php


namespace app\Interfaces;


interface ZipFilesInterface
{
    public function getList();
    public function extractToDir($pathFromFile);
}

