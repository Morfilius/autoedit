<?php


namespace app\Interfaces;


interface StorageInterface
{
    public function save($name);
    public function delete($name);
    public function get($name);
    public function has($name);
}