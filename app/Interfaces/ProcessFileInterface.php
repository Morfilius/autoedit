<?php


namespace app\Interfaces;


interface ProcessFileInterface
{
    public function process(array $fileGlobEl);
}