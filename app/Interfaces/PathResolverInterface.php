<?php


namespace app\Interfaces;


interface PathResolverInterface
{
    public function getBasePath();
    public function getFilesPath();
    public function getFilesWorkPath();
    public function getFileArchivePath();
    public function getViewsPath();
    public function getWorkUri();
}