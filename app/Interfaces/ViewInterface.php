<?php


namespace app\Interfaces;


interface ViewInterface
{
    public function render($view,array $args = array(), $status = 200, $ext = 'php');
    public function raw($str, $status = 200);
    public function json(array $array, $status = 200);
}