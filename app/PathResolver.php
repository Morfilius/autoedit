<?php


namespace app;


use app\Interfaces\PathResolverInterface;

class PathResolver implements PathResolverInterface
{
    private $baseDir;
    private $fileDirName;
    private $fileWorkDirName;
    private $fileArchiveTempDirName;
    private $viewsDirName;

    /**
     * PathResolver constructor.
     * @param $baseDir
     * @param $fileDirName
     * @param $fileWorkDirName
     * @param $fileArchiveTempDirName
     * @param $viewsDirName
     */
    public function __construct($baseDir, $fileDirName, $fileWorkDirName, $fileArchiveTempDirName, $viewsDirName)
    {
        $this->baseDir = $baseDir;
        $this->fileDirName = $fileDirName;
        $this->fileWorkDirName = $fileWorkDirName;
        $this->fileArchiveTempDirName = $fileArchiveTempDirName;
        $this->viewsDirName = $viewsDirName;
    }

    public function getBasePath()
    {
        return $this->baseDir.'/';
    }

    public function getFilesPath()
    {
        return $this->getBasePath().$this->fileDirName.'/';
    }

    public function getFilesWorkPath()
    {
        return $this->getFilesPath().$this->fileWorkDirName.'/';
    }

    public function getFileArchivePath()
    {
        return $this->getFilesWorkPath().$this->fileArchiveTempDirName.'/';
    }

    public function getViewsPath()
    {
        return $this->getBasePath().$this->viewsDirName.'/';
    }

    public function getWorkUri()
    {
        return '/'.$this->fileDirName.'/'.$this->fileWorkDirName.'/';
    }
}