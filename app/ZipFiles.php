<?php


namespace app;


use Alchemy\Zippy\Zippy;
use app\Interfaces\PathResolverInterface;
use app\Interfaces\ZipFilesInterface;

class ZipFiles implements ZipFilesInterface
{
    /**
     * @var Zippy
     */
    private $zippy;
    /**
     * @var PathResolverInterface
     */
    private $pathResolver;

    /**
     * ZipFiles constructor.
     * @param Zippy $zippy
     * @param PathResolverInterface $pathResolver
     */
    public function __construct(Zippy $zippy, PathResolverInterface $pathResolver)
    {
        $this->zippy = $zippy;
        $this->pathResolver = $pathResolver;
    }

    /**
     * @return array
     */
    public function getList()
    {
        $output = array();
        $archive = $this->zippy->open($this->getTempPath());

        foreach ($archive as $item){
            $output[] = $item->getLocation();
        }

        return $output;
    }

    /**
     * @param $pathFromFile
     * @return string
     */
    public function extractToDir($pathFromFile)
    {
        $archive = $this->zippy->open($pathFromFile);
        $archive->extract($this->getTempPath());

        return $this->getTempPath();
    }

    /**
     * @return string
     */
    private function getTempPath()
    {
        $pathTempDir = $this->pathResolver->getFileArchivePath();

        if (is_dir($pathTempDir)) {
            return $pathTempDir;
        }
        mkdir($pathTempDir,0755,true);

        return $pathTempDir;
    }
}