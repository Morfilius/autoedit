<?php


namespace app;


use app\Helpers\FileHelper;
use app\Interfaces\FileSaverInterface;
use app\Interfaces\TextHandlerFactoryInterface;
use app\Interfaces\PathResolverInterface;
use app\Interfaces\ProcessFileInterface;
use Exception;

class ProcessFile implements ProcessFileInterface
{
    private $saver;

    /**
     * @var PathResolverInterface
     */
    private $pathResolver;
    /**
     * @var TextHandlerFactoryInterface
     */
    private $factory;

    /**
     * ProcessFile constructor.
     * @param PathResolverInterface $pathResolver
     * @param FileSaverInterface $saver
     * @param TextHandlerFactoryInterface $factory
     */
    public function __construct(PathResolverInterface $pathResolver, FileSaverInterface $saver, TextHandlerFactoryInterface $factory)
    {
        $this->saver = $saver;
        $this->pathResolver = $pathResolver;
        $this->factory = $factory;
    }

    /**
     * @param array $fileGlobEl
     * @return string resultFilePath
     * @throws Exception
     */
    public function process(array $fileGlobEl)
    {
        $pathToFile = $this->saver->save($fileGlobEl);

        $ext = FileHelper::getFileExt($pathToFile);

        $handler = $this->factory->create($ext);
        $fileName = $handler->init($pathToFile);

        $this->saver->delete($this->pathResolver->getFilesWorkPath().$fileGlobEl['name']);

        return $this->pathResolver->getWorkUri().$fileName;
    }
}