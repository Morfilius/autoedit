<?php


namespace app;


use Psr\Container\ContainerInterface;

class Container extends \Pimple\Container implements ContainerInterface
{
    public function get($id)
    {
        return $this[$id];
    }

    public function has($id)
    {
        return isset($this[$id]);
    }
}