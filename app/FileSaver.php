<?php


namespace app;


use app\Interfaces\FileSaverInterface;
use app\Interfaces\PathResolverInterface;

class FileSaver implements FileSaverInterface
{
    /**
     * @var PathResolverInterface
     */
    private $pathResolver;

    /**
     * File constructor.
     * @param PathResolverInterface $pathResolver
     */
    public function __construct(PathResolverInterface $pathResolver)
    {
        $this->pathResolver = $pathResolver;
    }

    /**
     * @param array $fileGlobEl
     * @return string filename
     * @throws \Exception
     */
    public function save(array $fileGlobEl){

        if($fileGlobEl['error'] !== UPLOAD_ERR_OK)
            new \Exception('File not uploaded');

        $this->createDir();

        if(!move_uploaded_file($fileGlobEl['tmp_name'],$this->pathResolver->getFilesWorkPath().$fileGlobEl['name']))
            throw new \Exception('Failed to move file.');

        return $this->pathResolver->getFilesWorkPath().$fileGlobEl['name'];
    }

    /**
     * @param $pathToFile
     * @return bool
     */
    public function delete($pathToFile)
    {
        if(is_file($pathToFile)){
            return unlink($pathToFile);
        }

        return true;
    }

    /**
     * @param $id
     * @return string
     * @throws \Exception
     */
    private function createDir(){
        $workDir = $this->pathResolver->getFilesWorkPath();

        if(!is_dir($workDir)){
            if(!mkdir($workDir,0755)) throw new \Exception('Failed to create directory');
        }

        return $workDir;
    }
}