<?php


namespace app\AbstractClasses;


use app\DocumentParser;
use app\EditStream;
use app\Interfaces\DocumentParserInterface;
use app\Interfaces\PathResolverInterface;
use app\Interfaces\ZipFilesInterface;
use Exception;

abstract class AbstractTextHandler
{
    /**
     * @var DocumentParser
     */
    protected $parser;
    /**
     * @var EditStream
     */
    protected $edit;

    abstract function init($pathToFile);

    /**
     * @param EditStream $edit
     * @param DocumentParser $parser
     */

    public function __construct(EditStream $edit, DocumentParserInterface $parser)
    {
        $this->edit = $edit;
        $this->parser = $parser;
    }

    /**
     * @param $pathToFile
     * @return string
     * @throws Exception
     */
    protected function editContent($pathToFile)
    {
        return $this->edit->edit($this->getParsedContent($pathToFile));
    }

    /**
     * @param $pathToFile
     * @return string
     * @throws Exception
     */
    protected function getParsedContent($pathToFile)
    {
        return $this->parser::parseFromFile($pathToFile);
    }
}