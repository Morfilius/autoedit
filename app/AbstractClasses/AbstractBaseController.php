<?php


namespace app\AbstractClasses;


use app\Interfaces\ViewInterface;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractBaseController
{
    /**
     * @var ViewInterface
     */
    protected $view;

    public function __construct(ViewInterface $view)
    {
        $this->view = $view;
    }

    public function __call($name, $arguments) :ResponseInterface
    {
        // TODO: Implement __call() method.
        return call_user_func_array([$this,$name.'Action'],$arguments);
    }
}