<?php


namespace app\Handlers;


use app\AbstractClasses\AbstractTextHandler;
use app\AggregateFile;
use app\DocumentParser;
use app\EditStream;
use app\Interfaces\PathResolverInterface;
use Exception;

class TextFileHandler extends AbstractTextHandler
{
    /**
     * @var PathResolverInterface
     */
    private $pathResolver;

    /**
     * TextFileHandler constructor.
     * @param PathResolverInterface $pathResolver
     * @param EditStream $edit
     * @param DocumentParser $parser
     */
    public function __construct(PathResolverInterface $pathResolver, EditStream $edit, DocumentParser $parser)
    {
        parent::__construct($edit,$parser);
        $this->pathResolver = $pathResolver;
    }

    /**
     * @param $pathToFile
     * @return string
     * @throws Exception
     */
    public function init($pathToFile)
    {
        $aggregate = new AggregateFile($this->pathResolver->getFilesWorkPath(), $this->editContent($pathToFile));
        $aggregate->aggregate();
        return $aggregate->getResultFilename();
    }
}