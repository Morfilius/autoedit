<?php


namespace app\Handlers;


use app\AbstractClasses\AbstractTextHandler;
use app\AggregateFile;
use app\DocumentParser;
use app\EditStream;
use app\Interfaces\PathResolverInterface;
use app\Interfaces\ZipFilesInterface;
use Exception;

class ArchiveFileHandler extends AbstractTextHandler
{

    /**
     * @var PathResolverInterface
     */
    private $pathResolver;
    /**
     * @var ZipFilesInterface
     */
    private $zip;

    /**
     * TextFileHandler constructor.
     * @param PathResolverInterface $pathResolver
     * @param EditStream $edit
     * @param DocumentParser $parser
     * @param ZipFilesInterface $zip
     */
    public function __construct(PathResolverInterface $pathResolver, EditStream $edit, DocumentParser $parser, ZipFilesInterface $zip)
    {
        parent::__construct($edit,$parser);
        $this->pathResolver = $pathResolver;
        $this->zip = $zip;
    }

    /**
     * @param $pathToFile
     * @return string
     * @throws Exception
     */
    public function init($pathToFile)
    {
        $resultFilePath = '';
        $archiveFiles = $this->getSortFilesArchive($this->zip->extractToDir($pathToFile));

        foreach ($archiveFiles as $file){
            $pathToFile = $this->pathResolver->getFileArchivePath().$file;
            $aggregate = new AggregateFile($this->pathResolver->getFilesWorkPath(), $this->editContent($pathToFile));
            $aggregate->aggregate();
            $resultFilePath = $aggregate->getResultFilename();
            $this->deleteProcessedFile($pathToFile);
        }

        return $resultFilePath;
    }

    /**
     * @param $pathToFile
     * @throws Exception
     */
    private function deleteProcessedFile($pathToFile)
    {
        if(!unlink($pathToFile))
            throw new Exception('Unable to delete temporary file from archive in '.$pathToFile);
    }

    /**
     * @param $dir
     * @return array
     * @throws Exception
     */
    private function getSortFilesArchive($dir)
    {
        $sortByChapter = [];
        $files = array_diff(scandir($dir), ['.','..']);
        foreach ($files as $file){
            preg_match('/\d+/u',$file,$orderNumber);
            if (!empty($orderNumber)) {
                $index = $this->getIndex($sortByChapter, $orderNumber,  (int)reset($orderNumber)*10);
                $sortByChapter[$index] = $file;
            } else {
                $sortByChapter[] = $file;
            }
        }
        ksort($sortByChapter);

        return $sortByChapter;
    }

    private function getIndex($sortByChapter, $orderNumber, $index)
    {
        if(isset($sortByChapter[$index]))
        {
            $this->getIndex($sortByChapter, $orderNumber, $index += 1);
        }

        return $index;
    }
}