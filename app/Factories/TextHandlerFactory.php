<?php


namespace app\Factories;


use app\AbstractClasses\AbstractTextHandler;
use app\Handlers\ArchiveFileHandler;
use app\Handlers\TextFileHandler;
use app\Interfaces\TextHandlerFactoryInterface;
use app\Interfaces\TextHandlerInterface;
use Psr\Container\ContainerInterface;

class TextHandlerFactory implements TextHandlerFactoryInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * TextHandlerFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $fileExtension
     * @return TextHandlerInterface
     */
    public function create($fileExtension) : AbstractTextHandler
    {
        switch ($fileExtension) {
            case ('zip'):
                return new ArchiveFileHandler($this->container['app\PathResolver'], $this->container['app\EditStream'],
                    $this->container['app\DocumentParser'],$this->container['app\ZipFiles']);
            default:
                return new TextFileHandler($this->container['app\PathResolver'], $this->container['app\EditStream'],
                    $this->container['app\DocumentParser']);
        }
    }
}