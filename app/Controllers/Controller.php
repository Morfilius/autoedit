<?php


namespace app\Controllers;


use app\AbstractClasses\AbstractBaseController;
use app\Interfaces\ProcessFileInterface;
use Psr\Http\Message\ServerRequestInterface;


class Controller extends AbstractBaseController
{

    /**
     * @param ServerRequestInterface $request
     * @param array $args
     */
    public function indexAction(ServerRequestInterface $request, $args = array())
    {
        return $this->view->render('form');
    }

    /**
     * @param ServerRequestInterface $request
     * @param ProcessFileInterface $processFile
     * @throws \Exception
     */
    public function addAction(ServerRequestInterface $request,ProcessFileInterface $processFile)
    {
        if(!isset($_FILES['file'])) throw new \Exception('Файл не загружен');

        $outputFilePath = $processFile->process(reset($_FILES));

        $response = ['output_file' => $outputFilePath];

        //return $this->view->raw('{"jsonrpc" : "2.0", "result" : '.$outputFilePath.'}');
        return $this->view->json($response);
    }

    /**
     * @param $message
     * @param $code
     */
    public function errorAction($message, $code)
    {
        return $this->view->render('error',array(
            'message' => $message,
            'code'    => $code
        ),404);
    }
}