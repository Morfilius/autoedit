<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="/assets/css/jquery.ui.plupload.css" type="text/css" />
    <link rel="stylesheet" href="/assets/css/main.css" type="text/css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/assets/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.ui.plupload.min.js"></script>
    <script type="text/javascript" src="/assets/js/ru.js"></script>
    <script type="text/javascript" src="/assets/js/js.cookie.min.js"></script>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Авторедактор текста</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="entry-title">Автоматическая сборка и редактирование текстовых файлов</h1>
            <div id="uploader">
                <p>Your browser doesn't have HTML5 support.</p>
            </div>
            <div id="uploader"></div>
            <a id="link" class="link btn btn-success" href="#" download>Скачать файл</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Initialize the widget when the DOM is ready
    $(function() {
        const id = Date.now();
        Cookies.set('id',id);
        $("#uploader").plupload({
            // General settings
            runtimes : 'html5',
            url : "/add/",

            // Maximum file size
            max_file_size : '30mb',
            //chunk_size: '1mb',

            // Specify what files to browse for
            filters : [
                {title : "Zip files", extensions : "zip,docx,doc,txt"}
            ],

            // Rename files by clicking on their titles
            rename: true,

            // Sort files
            sortable: true,

            // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
            dragdrop: true,

            // Views to activate
            views: {
                list: true,
                thumbs: true, // Show thumbs
                active: 'thumbs'
            },
        });
        $('#uploader').on("uploaded", function(event, file, status) {
            const response = JSON.parse(file.result.response);
            if(!!response.output_file) {
                const link = $('#link');
                link.attr('href',response.output_file).css('display','inline-block');
            }
        });
    });
</script>
</body>
</html>
