<?php

require_once 'vendor/autoload.php';
require_once 'config.php';
$d = $container['app\PathResolver'];

use League\Route\Http\Exception\HttpExceptionInterface;
use League\Route\Strategy\ApplicationStrategy;
use League\Route\Router;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\ServerRequestFactory;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;

$request = ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$router = new Router;
$strategy = (new ApplicationStrategy)->setContainer($container);
$router->setStrategy($strategy);

// map a route

$router->map('GET','/', 'app\Controllers\Controller::index');

$router->map('POST','/add/', function (ServerRequestInterface $request,$args) use($container) {
    return $container['app\Controllers\Controller']->add($request,$container['app\ProcessFile']);
});

try{
    $response = $router->dispatch($request);
}catch (HttpExceptionInterface $exception){
    $response = $container['app\Controllers\Controller']->error($exception->getMessage(),$exception->getStatusCode());
}

// send the response to the browser
(new SapiEmitter)->emit($response);