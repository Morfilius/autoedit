<?php
use Alchemy\Zippy\Zippy;
use app\Factories\TextHandlerFactory;
use app\PathResolver;
use app\Container;
use app\Controllers\Controller;
use app\DocumentParser;
use app\EditStream;
use app\FileSaver;
use app\ProcessFile;
use app\Storages\CookieStorages;
use app\TemplateRenderer;
use app\ZipFiles;
use Zend\Diactoros\Response;

$container = new Container();

$container['root_dir'] = __DIR__;
$container['files_dir_name'] = 'files';
$container['views_dir_name'] = 'views';
$container['file_works_dir_name'] = function ($container) {
    return $container['Storage']->get('id');
};
$container['file_archive_temp_dir_name'] ='archive';


$container['Storage'] = function ($container) {
    return new CookieStorages();
};
$container['app\PathResolver'] = function ($container){
    return new PathResolver($container['root_dir'], $container['files_dir_name'], $container['file_works_dir_name'],
        $container['file_archive_temp_dir_name'],$container['views_dir_name']);
};
$container['Zend\Diactoros\Response'] = $container->factory(function ($container){
    return new Response();
});
$container['app\DocumentParser'] = function ($container){
    return new DocumentParser();
};
$container['app\TemplateRenderer'] = function ($container){
    return new TemplateRenderer($container['app\PathResolver'],$container['Zend\Diactoros\Response']);
};
$container['app\Controllers\Controller'] = function ($container){
    return new Controller($container['app\TemplateRenderer']);
};
$container['app\FileSaver'] = function ($container){
    return new FileSaver($container['app\PathResolver']);
};
$container['app\ZipFiles'] = function ($container){
    return new ZipFiles( Zippy::load(), $container['app\PathResolver']);
};
$container['app\ProcessFile'] = function ($container){
    return new ProcessFile($container['app\PathResolver'], $container['app\FileSaver'], $container['app\Factories\TextHandlerFactory']);
};
$container['app\EditStream'] = function ($container){
    return new EditStream();
};
$container['app\Factories\TextHandlerFactory'] = function ($container){
    return new TextHandlerFactory($container);
};